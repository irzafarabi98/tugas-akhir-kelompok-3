# Anggota Kelompok 3
- Denny Rizkiawan
- Irza Farabi

# Tema Project
- Digital Library
Digital Library merupakan aplikasi berbasis web dengan platform laravel yang dikembangkan untuk tugas akhir dari bootcamp laravel sanbercode. Proses Bisnis yang terdapat di Digital Library berupa penyediaan Buku-Buku untuk dibaca oleh pengguna seperti halnya perpustakaan pada umumnya namun berbasis elektronik.

# ERD
![ERD](ERD.png)

## LINK VIDEO DEMO:
https://drive.google.com/drive/folders/1mT9F9GfGZLdxLPkWUsO11mPa1kk9bPVa?usp=sharing

## LINK DEPLOY HOSTING:
https://laravel.digital-library.sanbercodeapp.com/login



