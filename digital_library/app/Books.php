<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $table = "books";
    protected $guarded = [];
    public function categories() {
        return $this->belongsToMany('App\Categories', 'book_category', 'books_id', 'categories_id');
    }
}
