<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function store(Request $request)
    {
        $profile = Profile::create([
            "full_name" => $request['full_name'],
            "gender" => $request['gender'],
            "birth_year" => $request['birth_year'],
            "occupation" => $request['occupation'],
            "address" => "",
            "user_id" => Auth::id()
        ]);
        return redirect('/books');
    }
}
