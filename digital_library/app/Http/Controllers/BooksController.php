<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use DB;
use App\Books;
use App\User;
use App\Categories;
use Auth;

class BooksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'store', 'edit', 'update', 'delete', 'index', 'show']);
    }
    public function index()
    {
        $books = Books::paginate(4);
        // $books = Books::all(); // Eloquent ORM
        // return view('books.index', compact('books'));
        return view('books.index', ['books' => $books]);
    }
    public function create() {
        return view('books.create');
    }
    public function store(Request $request) {
        $categories_arr = explode(',', $request["categories"]);
    // public function store(Request $request) {
    //     $categories_arr = explode(',',preg_replace('/\s+/', '', $request["categories"]));

        $category_ids = [];
        foreach($categories_arr as $category){
            $categories = Categories::where('category', $category)->first();
            if($categories) {
                $category_ids[] = $categories->id;
            } else {
                $new_category = Categories::create(['category' => $category]);
                $category_ids[] = $new_category->id;
            }
        }

        $imageName = time().'.'.$request['cover']->extension();  
        $image = $request['cover']->move(public_path('img/covers'), $imageName);
        $books = Books::create([
            "title" => $request['title'],
            "author" => $request['author'],
            "publisher" => $request['publisher'],
            "cover" => $imageName,
            "description" => $request['description'],
            "upload_date" => $request['upload_date'],
            "upload_time" => $request['upload_time'],
            "uploader" => $request['uploader'],
            "url" => $request['url']
        ]);
        $books->categories()->sync($category_ids);

        // dd($request->all());
        Alert::success('Success', 'New book added!');
        return redirect('/books');
    }
    public function show($id) {
        // $books = DB::table('books')->where('id',$id)->first();
        $books = Books::find($id);
        return view('books.show', compact('books'));
    }
    public function edit($id) {
        $books = Books::find($id);
        return view('books.edit', compact('books'));
    }
    public function update($id, Request $request) {
        $categories_arr = explode(',', $request["categories"]);
        $category_ids = [];
        foreach($categories_arr as $category){
            $categories = Categories::where('category', $category)->first();
            if($categories) {
                $category_ids[] = $categories->id;
            } else {
                $new_category = Categories::create(['category' => $category]);
                $category_ids[] = $new_category->id;
            }
        }
        $update = Books::where('id', $id)->update([
            "title" => $request['title'],
            "author" => $request['author'],
            "publisher" => $request['publisher'],
            "description" => $request['description'],
            "url" => $request['url']
        ]);
        Alert::success('Success', 'Book updated!');
        return redirect('/books'); 
    }
    public function destroy($id) {
        $query = DB::table('book_category')->where('books_id', $id)->delete();
        Books::destroy($id);
        return redirect('/books')->with('success', 'books deleted successfully!');
    }
}
