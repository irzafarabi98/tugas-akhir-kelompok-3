<?php

namespace App\Http\Controllers;

use DB;
use App\Profile;
use Illuminate\Http\Request;
use Auth;

class ProfilesController extends Controller
{
    public function index(){
      $profile = Auth::user()->profile;
      return view('profile.index', compact('profile'));
    }
    public function show($id) {
        $profile = Auth::user()->profile;
        return view('profile.index', compact('profile'));
    }
    public function update($id, Request $request) {
        $update = Profile::where('id', $id)->update([
            "full_name" => $request['full_name'],
            "gender" => $request['gender'],
            "occupation" => $request['occupation'],
            "address" => $request['address']
        ]);
        return redirect('/profile')->with('success', 'profiles created successfully!'); 
    }
}
