<?php

namespace App\Http\Controllers;
use DB;

use App\User;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index(){
      $user = User::all();
      return view('user', compact('user'));
    }
    public function destroy($id) {
        User::destroy($id);
        return redirect('/dashboard')->with('success', 'users deleted successfully!');
    }
}
