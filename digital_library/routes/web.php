<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/books'); 
});
Route::get('/dashboard', function () {
    $users = DB::select('select * from users');
    return view('dashboard', [
        'users' => $users
    ]);
});
// Route::any('/search', function(){
//     $key = Request::input('key');
//     if($key != ''){
//         $books = Books::where('title', 'LIKE', '%'.$key.'%key')
//                         ->orWhere('author', 'LIKE', '%'.$key.'%key')
//                         ->orWhere('description', 'LIKE', '%'.$key.'%key')
//                         ->paginate(4)
//                         ->setpath('');
//     }
// });
Route::resource('books', 'BooksController')/*->middleware('auth')*/;
Route::resource('users', 'UsersController');
Route::resource('profile', 'ProfilesController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@store');
