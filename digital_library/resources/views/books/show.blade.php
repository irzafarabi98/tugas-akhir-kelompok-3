@extends('adminlte.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ $books->title }} Details</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Books</li>
            <li class="breadcrumb-item active">Show</li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div id="books_thumb" class="card-body row padd-40 vertical-center">
<!-- Content starts here -->


                        <div class="col-md-3 padd-20">
                            <img src="{{ asset('img/covers/'.$books->cover) }}">
                        </div>
                        <div class="col-md-9 padd-20">
                            <div class="card">
                                <div class="card-body">
                                    <h5>{{ $books->title }}</h5>
                                    <p class="mb-2">{{ $books->author }} | {{ $books->publisher }}</p>
                                    <p class="text-sub mb-2">{{ $books->upload_date }}&nbsp; {{ $books->upload_time }}</p>
                                    <p class="text-sub">{{ $books->description }}</p>
                                    <p><a href="{{ $books->url }}" target="_blank">Read &raquo;</a></p>
                                </div>
                                <div class="card-footer ptb-20">
                                    <p>Categories :</p>
                                    <p>@forelse($books->categories as $category)
                                        <span class="btn btn-primary btn-sm">{{ $category->category }}</span>
                                        @empty
                                        <span class="btn btn-primary btn-sm">No category</span>
                                    @endforelse</p>
                                </div>
                            </div>
                            <div class="card">
                                <form action="/books/{{ $books->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <div class="card-body">
                                    <a href="{{ route('books.edit', $books->id) }}">
                                        <span class="btn btn-outline-primary mr-10">Edit</span>
                                    </a>
                                    <input type="submit" value="Delete" class="btn btn-outline-danger mr-10" onclick="return confirm('Are you sure you want to delete this book?');">
                                </div>
                                </form>
                            </div>
                        </div>




<!-- Content ends here -->
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /Row -->

    </div>
</section>

@endsection
@push('scripts')
@endpush
