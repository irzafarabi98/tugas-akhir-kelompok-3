@extends('adminlte.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Edit Books {{ $books->title }}</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Books</li>
            <li class="breadcrumb-item active">Edit</li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div id="books_thumb" class="card-body row padd-40 vertical-center">
<!-- Content starts here -->


                        <div class="col-md-3 padd-20">
                            <img src="{{ asset('img/covers/'.$books->cover) }}">
                        </div>
                        <div class="col-md-9 padd-20">
                            <form action="/books/{{ $books->id }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="card">
                                <div class="card-body">
                                    Title : <textarea class="form-control form-input mb-20" name="title" books="5" required>{{ $books->title}}</textarea>
                                    Author : <input class="form-control form-input mb-20" type="text" name="author" value="{{ $books->author }}">
                                    Publisher : <input class="form-control form-input mb-20" type="text" name="publisher" value="{{ $books->publisher }}">
                                    Description : <textarea rows="5" class="form-control form-input mb-20" name="description" required>{{ $books->description }}</textarea>
                                    Categories : <input name="categories" type="text" class="form-control mb-20" id="category" placeholder="Categories" value="{{ old('category', '') }}">
                                    URL : <input class="form-control form-input mb-40" type="text" name="url" value="{{ $books->url }}" required>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body d-flex">
                                    <button type="submit" class="btn btn-primary mr-10">Update</button>
                                    </form>
                                    <form action="/books/{{ $books->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="Delete" class="btn btn-outline-danger mr-10" onclick="return confirm('Are you sure you want to delete this book?');">
                                    </form>
                                </div>
                            </div>
                        </div>




<!-- Content ends here -->
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /Row -->

    </div>
</section>

@endsection
@push('scripts')
@endpush
