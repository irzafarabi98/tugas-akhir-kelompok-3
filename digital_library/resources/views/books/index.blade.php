@extends('adminlte.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Books Index</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Books</li>
            <li class="breadcrumb-item active">Index</li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
<!-- Content starts here -->

                    <div class="card-header ptb-20">
                            <div class="input-group">
                                <input type="text" class="form-control mr-15" name="key" placeholder="Search">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default">Search</button>
                                </span>
                            </div>
                    </div>

                    <div id="books_thumb" class="card-body row padd-40">
                    @foreach($books as $book)
                        <div class="col-md-3 padd-20">
                            <div class="card">
                                    <img src="{{ asset('img/covers/'.$book->cover) }}">
                                <div class="card-body">
                                    <a href="{{ route('books.show', $book->id) }}">
                                    <h5>{{ $book->title }}</h5>
                                    </a>
                                    <p class="mb-2">{{ $book->author }} | {{ $book->publisher }}</p>
                                    <p class="text-sub">{{ $book->upload_date }}</p>
                                    <p><a href="{{ $book->url }}" target="_blank">Read &raquo;</a></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>

<!-- Content ends here -->
                    <!-- /.card-body -->
                    <div class="card-footer ptb-30">
                        {{ $books->links() }}
                    </div>
                    <!-- /.card-footer -->
                </div>
            </div>
        </div>
        <!-- /Row -->

    </div>
</section>

@endsection
@push('scripts')
<script>
$(document).ready( function () {
    $('.pagination').addClass('justify-content-center mb-0');
});
</script>
@endpush
