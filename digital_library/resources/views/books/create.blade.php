@extends('adminlte.master')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Submit a New Book</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Books</li>
            <li class="breadcrumb-item active">Index</li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div id="books_thumb" class="card-body row padd-40 vertical-center">
<!-- Content starts here -->


                        <div class="col-md-3 padd-20">
                        <form role="form" action="/books" enctype="multipart/form-data" method="POST">
                        @csrf
                            <img id="img_preview" class="mb-40 box-shadow-1" src="{{ asset('img/covers/sample.jpg') }}">
                            <input class="form-control form-input mb-20" type="file" name="cover" onchange="readURL(this);">
                            <img id="img_preview" src="">
                        </div>
                        <div class="col-md-9 padd-20">

              <!-- form start -->
              <div class="card">
                <div class="card-body">
                  <div class="form-group">
                    <input type="hidden" name="upload_date" value="<?= date("Y-m-d"); ?>">
                    <input type="hidden" name="upload_time" value="<?= date('h:i'); ?>">
                    <input type="hidden" name="uploader" value="{{ Auth::user()->id }}">
                    <label for="Inputtitle">Title</label>
                    <input name="title" type="name" class="form-control" id="Inputtitle" placeholder="Title" value="{{ old('title', '') }}">
                    @error('title')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="Author">Author</label>
                    <input name="author" type="text" class="form-control" id="Author" placeholder="Author" value="{{ old('author', '') }}">
                    @error('author')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="Publisher">Publisher</label>
                    <input name="publisher" type="text" class="form-control" id="Publisher" placeholder="Publisher" value="{{ old('publisher', '') }}">
                    @error('publisher')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group"> <!-- description -->
                    <label for="Inputdescription">Description</label>
                    <textarea name="description" class="form-control" id="Inputdescription" placeholder="Description">{{ old('description', '') }}</textarea>
                    @error('description')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group"> <!-- Category -->
                    <label for="Inputcategory">Categories</label>
                    <input name="categories" type="text" class="form-control" id="category" placeholder="Categories" value="{{ old('category', '') }}">
                    @error('category')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group mb-40"> <!-- url -->
                    <label for="Inputurl">Url</label>
                    <input name="url" type="text" class="form-control" id="Url" placeholder="Url" value="{{ old('Url', '') }}">
                    @error('url')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
                <!-- /.card-body -->

              </form>
                </div>
            </div>




<!-- Content ends here -->
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /Row -->

    </div>
</section>

@endsection
@push('scripts')
<script src="https://cdn.tiny.cloud/1/azk02t135qrzd1zf1kmlzrv6alvdkzlaq3q0kn01d0ce8mu9/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
</script>
<script type="text/javascript">
    AOS.init({ once: true,  easing: 'ease-in-out-sine' });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endpush
