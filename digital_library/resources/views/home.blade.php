@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                    <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

            <!-- Form start -->
            <form method="POST" action="{{ route('home') }}">
                        @csrf
                        <div class="form-group row"> <!-- full_name -->
                            <label for="full_name" class="col-md-4 col-form-label text-md-right">Full Name</label>
                            <div class="col-md-6">
                                <input id="full_name" type="text" class="form-control @error('name') is-invalid @enderror" name="full_name" value="{{ old('full_name') }}" required autocomplete="full_name" autofocus>
                                @error('full_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row"> <!-- gender -->
                            <label for="gender" class="col-md-4 col-form-label text-md-right">Gender</label>
                            <div class="col-md-6">
                                <select name="gender" class="form-control mb-10" required>
					                <option value="" disabled selected><span class="text-muted">Select</span></option>
                                    <option id="male" value="male">Male</option>
                                    <option id="female" value="female">Female</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row"> <!-- birth_year -->
                            <label for="birth_year" class="col-md-4 col-form-label text-md-right">Birth year</label>
                            <div class="col-md-6">
                                <input id="birth_year" type="text" class="form-control @error('birth_year') is-invalid @enderror" name="birth_year" value="{{ old('birth_year') }}" required autocomplete="birth_year" autofocus>
                                @error('birth_year')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row"> <!-- occupation -->
                            <label for="occupation" class="col-md-4 col-form-label text-md-right">Occupation</label>
                            <div class="col-md-6">
                                <input id="occupation" type="text" class="form-control @error('occupation') is-invalid @enderror" name="occupation" value="{{ old('occupation') }}" required autocomplete="occupation" autofocus>
                                @error('occupation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0"> <!-- Submit -->
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit Profile
                                </button>
                            </div>
                        </div>
                        
                    </form> <!-- End form -->

                </div> <!-- Card body -->
            </div>
        </div>
    </div>
</div>
@endsection
